<?php
require "fonctions.php";

$empty_page = function ($param = null) {
    Flight::render("templates/base.tpl");
};

if (isset($_SESSION['logged']) && $_SESSION['logged']) {
    if (isset($_SESSION['admin']) && $_SESSION['admin']) {
        /** ADMIN **/
        
        Flight::route("/", $admin_index);
        Flight::route("/users", $list_users);
        Flight::route("/candidatures", $list_candidature);
        Flight::route("/candidature/@group_id", $show_candidature);

        Flight::route("/profil/@username", $profil);
    } else {
        /** CANDIDAT **/

        Flight::route("/", $goto_profil);
        
        Flight::route("GET /candidature", $candidature['home']['get']);
        Flight::route("GET /candidature/1", $candidature['group']['get']);
        Flight::route("GET /candidature/2", $candidature['membres']['get']);
        Flight::route("GET /candidature/3", $candidature['infos']['get']);
        
        Flight::route("POST /candidature", $candidature['home']['post']);
        Flight::route("POST /candidature/1", $candidature['group']['post']);
        Flight::route("POST /candidature/2", $candidature['membres']['post']);
        Flight::route("POST /candidature/3", $candidature['infos']['post']);
    }
    /** TOUT CONNECTÉ **/
    
    Flight::route("/logout", $logout);
    Flight::route("/profil", $profil);
} else {
    /** NON CONNECTÉ **/

    Flight::route("/", $home);
    
    Flight::route("GET /login", $get_login);
    Flight::route("GET /register", $get_login);
    
    Flight::route("POST /login", $login);
    Flight::route("POST /register", $register);
}

/** BONUS **/

Flight::route("/stats/nombre-candidatures", $stats_all);
Flight::route("/stats/candidatures-par-departement", $stats_all_depts);
Flight::route("/stats/candidatures-par-departement/@dept", $stats_dept);
Flight::route("/stats/candidatures-hauts-de-france", $stats_hdf);
Flight::route("/stats/candidatures-hors-hauts-de-france", $stats_nhdf);
Flight::route("/stats/candidatures-par-scene", $stats_scenes);
Flight::route("/stats/candidatures-par-scene/@scene", $stats_scene);
